﻿using System;
using System.Diagnostics;
using System.IO;
using System.Threading;
using RestBin.ClientEndpoint.Proxy;
using RestBin.Common.PInvoke;
using RestBin.Common.Utils;

namespace RestBin.Cmd
{
    internal static class Program
    {
        private const string BASE_URL = "http://localhost:8899"; 

        private static void Main(string[] args)
        {
            Console.WriteLine("--------------------------------");
            Console.WriteLine("---- Rest binary file tool ----");
            Console.WriteLine("--------------------------------");
            Console.WriteLine();
            Console.WriteLine("1 - Generate");
            Console.WriteLine("2 - Upload");
            Console.WriteLine("3 - Download");
            Console.WriteLine("4 - Get record by id");
            Console.WriteLine("5 - Delete record by id");
            Console.WriteLine("0 - Clear console");
            Console.WriteLine();

            while (true)
            {
                var choose = Console.ReadLine();

                if (string.IsNullOrWhiteSpace(choose) || choose == "0")
                {
                    Console.Clear();

                    continue;
                }

                switch (choose.ToLowerInvariant())
                {
                    case "1":
                        {
                            TestMethod_00();
                        }
                        break;

                    case "2":
                        {
                            Console.WriteLine("Please enter remote binary file:");
                            var file = Console.ReadLine();

                            TestMethod_01(file);
                        }
                        break;

                    case "3":
                        {
                            TestMethod_02();
                        }
                        break;

                    case "4":
                        {
                            Console.WriteLine("Please enter record[id]:");
                            var id = Convert.ToInt32(Console.ReadLine());

                            TestMethod_03(id);
                        }
                        break;

                    case "5":
                        {
                            Console.WriteLine("Please enter record[id]:");
                            var id = Convert.ToInt32(Console.ReadLine());

                            TestMethod_04(id);
                        }
                        break;
                }

                Thread.Sleep(1000);
            }
        }


        /// <summary>
        ///     Generate binary file
        /// </summary>
        private static void TestMethod_00()
        {
            var header = new Header
            {
                version = 1,
                type = "type1"
            };
            var records = new TradeRecord[64];
            for (var i = 0; i < records.Length; i++)
                records[i] = new TradeRecord
                {
                    account = i + 1,
                    id = i,
                    volume = i * 10d,
                    comment = "comment" + i
                };

            var bytes = StructureParser.Serialize(header, records);
            var binFile = Path.Combine(Environment.CurrentDirectory, "myDataStruct.bin");
            File.WriteAllBytes(binFile, bytes);

            Console.WriteLine("Test binary file '{0}' is created!", binFile);

            try
            {
                Process.Start(binFile);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                throw;
            }
        }

        /// <summary>
        ///     Upload binary
        /// </summary>
        private static void TestMethod_01(string file)
        {
            var importClient = new ImportApiEndpoint(BASE_URL);
            var result = importClient.Post(file);

            if (result)
                Console.WriteLine("Binary file is uploaded.");
        }

        /// <summary>
        ///     Export excel
        /// </summary>
        private static void TestMethod_02()
        {
            var exportClient = new ExportApiEndpoint(BASE_URL);
            var data = exportClient.Get();
            var exportFile = Path.Combine(Environment.CurrentDirectory, "test.xlsx");

            File.WriteAllBytes(exportFile, data);

            Console.WriteLine("Excel file '{0}' is ready!", exportFile);

            try
            {
                Process.Start(exportFile);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                throw;
            }
        }

        /// <summary>
        ///     Get record by id
        /// </summary>
        private static void TestMethod_03(int id)
        {
            var recordClient = new RecordApiEndpoint(BASE_URL);
            var data = recordClient.Get(id);

            if (data != null)
                Console.WriteLine("Record#{0} is ready", data.Id);
        }

        /// <summary>
        ///     Delete record by id
        /// </summary>
        private static void TestMethod_04(int id)
        {
            var recordClient = new RecordApiEndpoint(BASE_URL);

            if (recordClient.Delete(id))
                Console.WriteLine("Deleting failed");
        }
    }
}